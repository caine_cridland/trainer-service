package com.thortful.trainerservice.service;

import com.thortful.trainerservice.client.PokemonClient;
import com.thortful.trainerservice.model.Pokemon;
import com.thortful.trainerservice.model.Trainer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class PokemonServiceTests {

    private PokemonService service;

    @Mock
    private PokemonClient pokemonClient;

    private Pokemon returnedPokemon;

    @Before
    public void setup() {
        returnedPokemon = Pokemon.builder()
                .id(82)
                .name("magneton")
                .sprites(Pokemon.PokemonSprites.builder().frontDefault("example.com").build())
                .build();

        when(pokemonClient.getPokemon(anyInt())).thenReturn(returnedPokemon);

        service = new PokemonService(pokemonClient);
    }

    @Test
    public void test_enhanceTrainerWithPokemonData() throws ExecutionException, InterruptedException {
        var trainer = Trainer.builder()
                .firstname("firstname")
                .lastname("lastname")
                .pokemonId1(1)
                .pokemonId2(2)
                .pokemonId3(3)
                .pokemonId4(4)
                .pokemonId5(5)
                .pokemonId6(6)
                .build();
        var resultFuture = service.enhanceTrainerWithPokemonData(trainer);
        var result = resultFuture.get();
        assertTrue(result.getPokemonList().size() == 6);
    }
}
