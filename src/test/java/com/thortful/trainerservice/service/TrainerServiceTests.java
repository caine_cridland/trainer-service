package com.thortful.trainerservice.service;

import com.thortful.trainerservice.model.Trainer;
import com.thortful.trainerservice.repository.TrainerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class TrainerServiceTests {

    private TrainerService service;

    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private PokemonService pokemonService;

    private Trainer returnedTrainer;

    @Before
    public void setup() {
        returnedTrainer = Trainer.builder()
                .id(123l)
                .firstname("firstname")
                .lastname("lastname")
                .pokemonId1(1)
                .pokemonId2(2)
                .pokemonId3(3)
                .pokemonId4(4)
                .pokemonId5(5)
                .pokemonId6(6)
                .build();
        when(pokemonService.enhanceTrainerWithPokemonData(any())).thenAnswer(i -> CompletableFuture.completedFuture(i.getArguments()[0]));
        when(trainerRepository.save(Mockito.any(Trainer.class))).thenAnswer(i -> i.getArguments()[0]);
        when(trainerRepository.findById(any())).thenReturn(Optional.ofNullable(returnedTrainer));
        service = new TrainerService(trainerRepository, pokemonService,151,1);
    }

    @Test
    public void test_createTrainer() throws ExecutionException, InterruptedException {
        var firstname = "firstname";
        var lastname = "lastname";
        var newTrainer = Trainer.builder()
                .firstname(firstname)
                .lastname(lastname)
                .build();

        var resultFuture = service.createTrainer(newTrainer);
        var result = resultFuture.get();

        assertTrue(result.getFirstname().equals(firstname));
        assertTrue(result.getLastname().equals(lastname));
        assertNotNull(result.getPokemonId1());
        assertNotNull(result.getPokemonId2());
        assertNotNull(result.getPokemonId3());
        assertNotNull(result.getPokemonId4());
        assertNotNull(result.getPokemonId5());
        assertNotNull(result.getPokemonId6());
    }

    @Test
    public void test_updateTrainer() throws ExecutionException, InterruptedException {
        var firstname = "NEWfirstname";
        var lastname = "NEWlastname";
        var newTrainer = Trainer.builder()
                .id(123l)
                .firstname(firstname)
                .lastname(lastname)
                .build();

        var resultFuture = service.updateTrainer(123l, newTrainer);
        var result = resultFuture.get();

        assertTrue(result.getFirstname().equals(firstname));
        assertTrue(result.getLastname().equals(lastname));
        assertNotNull(result.getPokemonId1());
        assertNotNull(result.getPokemonId2());
        assertNotNull(result.getPokemonId3());
        assertNotNull(result.getPokemonId4());
        assertNotNull(result.getPokemonId5());
        assertNotNull(result.getPokemonId6());
    }
}
