DROP TABLE IF EXISTS trainers;

CREATE TABLE trainers (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  version INT DEFAULT 0 NOT NULL,
  firstname VARCHAR(250) NOT NULL,
  lastname VARCHAR(250) NOT NULL,
  pokemon_id_1 INT NOT NULL,
  pokemon_id_2 INT NOT NULL,
  pokemon_id_3 INT NOT NULL,
  pokemon_id_4 INT NOT NULL,
  pokemon_id_5 INT NOT NULL,
  pokemon_id_6 INT NOT NULL
);