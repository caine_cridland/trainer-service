package com.thortful.trainerservice.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "trainers")
public class Trainer {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Version
    @Column(name = "version", nullable = false)
    private Long version;

    @NotNull
    @Length(min = 1, max = 250)
    @Column(name = "firstname", nullable = false)
    private String firstname;

    @NotNull
    @Length(min = 1, max = 250)
    @Column(name = "lastname", nullable = false)
    private String lastname;

    @NotNull
    @Column(name = "pokemon_id_1", nullable = false)
    private Integer pokemonId1;

    @NotNull
    @Column(name = "pokemon_id_2", nullable = false)
    private Integer pokemonId2;

    @NotNull
    @Column(name = "pokemon_id_3", nullable = false)
    private Integer pokemonId3;

    @NotNull
    @Column(name = "pokemon_id_4", nullable = false)
    private Integer pokemonId4;

    @NotNull
    @Column(name = "pokemon_id_5", nullable = false)
    private Integer pokemonId5;

    @NotNull
    @Column(name = "pokemon_id_6", nullable = false)
    private Integer pokemonId6;

    @Transient
    private List<Pokemon> pokemonList;
}
