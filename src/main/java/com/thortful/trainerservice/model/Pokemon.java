package com.thortful.trainerservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Pokemon {
    private Integer id;
    private String name;
    private PokemonSprites sprites;

    @Data
    @Builder
    public static class PokemonSprites {
        @JsonProperty("front_default")
        private String frontDefault;
    }
}
