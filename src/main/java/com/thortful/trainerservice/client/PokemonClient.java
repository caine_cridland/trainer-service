package com.thortful.trainerservice.client;

import com.thortful.trainerservice.model.Pokemon;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * A client used for calling the PokeAPI.
 */
@Component
@Slf4j
public class PokemonClient {

    private RestTemplate restTemplate;
    private String baseUrl;

    @Autowired
    public PokemonClient(RestTemplate restTemplate,
                         @Value("${pokemon.api.baseUrl}") String baseUrl) {
        this.restTemplate = restTemplate;
        this.baseUrl = baseUrl;
    }

    /**
     * Retrieves the data for one pokemon and caches it for one day.
     * @param id The id of the pokemon.
     * @return The data of the pokemon.
     */
    @Cacheable("pokemon")
    public Pokemon getPokemon(int id) {
        log.info("Requesting info for pokemon id={}", Integer.toString(id));
        String url = baseUrl.concat("pokemon/").concat(Integer.toString(id));
        return restTemplate.getForObject(url,Pokemon.class);
    }
}
