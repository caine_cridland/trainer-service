package com.thortful.trainerservice.web;

import com.thortful.trainerservice.model.Trainer;
import com.thortful.trainerservice.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(value = "api/trainers")
public class TrainerController {

    private TrainerService trainerService;

    @Autowired
    public TrainerController(TrainerService trainerService) {
        this.trainerService = trainerService;
    }

    /**
     * Endpoint for creating a new trainer. 6 Pokemon are assigned at random.
     * @param trainer An object containing the trainer's firstname and lastname.
     * @return Returns the created trainer.
     */
    @PostMapping
    public CompletableFuture<Trainer> postTrainer(@RequestBody @Validated Trainer trainer) {
        return trainerService.createTrainer(trainer);
    }

    /**
     * Endpoint for retrieving an existing trainer.
     * @param id The Id of the trainer.
     * @return Returns the desired trainer.
     */
    @GetMapping(value = "/{id}")
    public CompletableFuture<Trainer> getTrainer(@PathVariable("id") Long id) {
        return trainerService.getTrainer(id);
    }

    /**
     * Endpoint for updating the name of a trainer.
     * @param id The Id of the trainer.
     * @param trainer An object containing the trainer's new firstname and lastname.
     * @return Returns the updated trainer.
     */
    @PutMapping(value = "/{id}")
    public CompletableFuture<Trainer> putTrainer(@PathVariable("id") Long id, @RequestBody @Validated Trainer trainer) {
        return trainerService.updateTrainer(id, trainer);
    }

    /**
     * Endpoint for deleting a trainer.
     * @param id The Id of the trainer.
     * @return
     */
    @DeleteMapping(value = "/{id}")
    public CompletableFuture<ResponseEntity> deleteTrainer(@PathVariable("id") Long id) {
        return trainerService.deleteTrainer(id);
    }

}
