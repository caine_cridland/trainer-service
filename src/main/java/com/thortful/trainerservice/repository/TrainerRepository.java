package com.thortful.trainerservice.repository;

import com.thortful.trainerservice.model.Trainer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainerRepository extends JpaRepository<Trainer, Long> {
}
