package com.thortful.trainerservice.service;

import com.thortful.trainerservice.client.PokemonClient;
import com.thortful.trainerservice.model.Trainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Service in charge of enhancing trainers with Pokemon data.
 */
@Service
public class PokemonService {

    private PokemonClient pokemonClient;

    @Autowired
    public PokemonService(PokemonClient pokemonClient) {
        this.pokemonClient = pokemonClient;
    }

    /**
     * Enhances a trainer object with data on the pokemon associated with it.
     * @param trainer The trainer to be updated with pokemon data.
     * @return The updated trainer.
     */
    public CompletableFuture<Trainer> enhanceTrainerWithPokemonData(Trainer trainer) {
        var pokemonDataList = List.of(
            pokemonClient.getPokemon(trainer.getPokemonId1()),
            pokemonClient.getPokemon(trainer.getPokemonId2()),
            pokemonClient.getPokemon(trainer.getPokemonId3()),
            pokemonClient.getPokemon(trainer.getPokemonId4()),
            pokemonClient.getPokemon(trainer.getPokemonId5()),
            pokemonClient.getPokemon(trainer.getPokemonId6())
        );

        trainer.setPokemonList(pokemonDataList);

        return CompletableFuture.completedFuture(trainer);
    }
}
