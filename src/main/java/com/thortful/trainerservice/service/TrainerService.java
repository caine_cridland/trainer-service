package com.thortful.trainerservice.service;

import com.thortful.trainerservice.exception.NotFoundException;
import com.thortful.trainerservice.model.Trainer;
import com.thortful.trainerservice.repository.TrainerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

/**
 * Service class in charge of CRUD operations on Trainers.
 */
@Service
@Slf4j
public class TrainerService {

    private TrainerRepository trainerRepository;
    private PokemonService pokemonService;

    private int pokemonIdLimitMax;
    private int pokemonIdLimitMin;

    @Autowired
    public TrainerService(TrainerRepository trainerRepository,
                          PokemonService pokemonService,
                          @Value("${pokemon.id.limit.max}") int pokemonIdLimitMax,
                          @Value("${pokemon.id.limit.min}") int pokemonIdLimitMin) {
        this.trainerRepository = trainerRepository;
        this.pokemonService = pokemonService;
        this.pokemonIdLimitMax = pokemonIdLimitMax;
        this.pokemonIdLimitMin = pokemonIdLimitMin;
    }

    /**
     * Creates a trainer and saves it to the database.
     * @param trainer The trainer to be created in the database.
     * @return Returns the created trainer.
     */
    public CompletableFuture<Trainer> createTrainer(Trainer trainer) {
        return enhanceWithPokemonIds(trainer)
                .thenCompose(enhancedTrainer -> pokemonService.enhanceTrainerWithPokemonData(enhancedTrainer))
                .thenApply(enhancedTrainerWithPokemon -> trainerRepository.save(enhancedTrainerWithPokemon));
    }

    /**
     * Adds the Ids of 6 random pokemon to the trainer.
     * @param trainer The trainer to be updated.
     * @return The updated trainer.
     */
    private CompletableFuture<Trainer> enhanceWithPokemonIds(Trainer trainer) {
        trainer.setPokemonId1(getRandomPokemonId());
        trainer.setPokemonId2(getRandomPokemonId());
        trainer.setPokemonId3(getRandomPokemonId());
        trainer.setPokemonId4(getRandomPokemonId());
        trainer.setPokemonId5(getRandomPokemonId());
        trainer.setPokemonId6(getRandomPokemonId());

        return CompletableFuture.completedFuture(trainer);
    }

    /**
     * Gets a random Id between the two values in the application properties file.
     * @return The randomly generated id.
     */
    private int getRandomPokemonId() {
        return (int)(Math.random() * ((pokemonIdLimitMax - pokemonIdLimitMin) + 1)) + pokemonIdLimitMin;
    }

    /**
     * Retrieves a trainer fromt he database.
     * @param id The id of the trainer.
     * @return The retrieved trainer.
     */
    public CompletableFuture<Trainer> getTrainer(long id) {
        var trainerOpt = trainerRepository.findById(id);
        if (trainerOpt.isEmpty()) {
            log.error("Trainer not found with id={}", id);
            throw new NotFoundException();
        }
        return pokemonService.enhanceTrainerWithPokemonData(trainerOpt.get());
    }

    /**
     * Updates a trainer's name in the database.
     * @param id The id of the trainer.
     * @param trainer The object containing the trainer's updated name.
     * @return The updated trainer.
     */
    public CompletableFuture<Trainer> updateTrainer(long id, Trainer trainer) {
        var trainerOpt = trainerRepository.findById(id);
        if (trainerOpt.isEmpty()) {
            log.error("Trainer not found with id={}", id);
            throw new NotFoundException();
        }
        trainer.setId(id);
        var updatedTrainer = trainerOpt.get();
        updatedTrainer.setFirstname(trainer.getFirstname());
        updatedTrainer.setLastname(trainer.getLastname());
        return pokemonService.enhanceTrainerWithPokemonData(updatedTrainer)
                .thenApply(enhancedTrainerWithPokemon -> trainerRepository.save(enhancedTrainerWithPokemon));
    }

    /**
     * Deletes a trainer from the database.
     * @param id The id of the trainer to be deleted.
     * @return
     */
    public CompletableFuture<ResponseEntity> deleteTrainer(long id) {
        var trainerOpt = trainerRepository.findById(id);
        if (trainerOpt.isEmpty()) {
            log.error("Trainer not found with id={}", id);
            throw new NotFoundException();
        }
        trainerRepository.deleteById(id);
        return CompletableFuture.completedFuture(ResponseEntity.ok().build());
    }
}
