# trainer-service
A service for handling CRUD operations on Pokemon trainers.
Pokemon details are retrieved from the PokeAPI (https://pokeapi.co/).

## Running The Service

Requirements:
- Java 11
- Maven

To build the application, run `mvn clean install` in the root directory. This will also run all unit tests.
To run the application, run `mvn spring-boot:run` in the root directory.

To view the database while the service is running, visit `http://localhost:8080/h2-console` and use the following credentials:
```
JDBC URL: jdbc:h2:mem:testdb
Username: username
Password: password
```

The healthcheck for the service can be accessed from `http://localhost:8080/actuator/health`.

## Endpoints
- POST: localhost:8080/api/trainers - Creates a new trainer and assigns them 6 random Pokemon.
```
Example body:
{
    "firstname": "Caine",
    "lastname": "Cridland"
}
```

- GET: localhost:8080/api/trainers/{id} - Retrieves a Trainer.

- PATCH: localhost:8080/api/trainers/{id} - Updates a Trainer's name.
```
Example body:
{
    "firstname": "Homer",
    "lastname": "Simpson"
}
```

- DELETE: localhost:8080/api/trainers/{id} - Deletes a Trainer.
